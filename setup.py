"""
This is a setup.py script for The Screenless Office

"""
from setuptools import setup

VERSION = "0.1"

setup(name="screenless",
      packages = ["screenless"],
      version = VERSION,
      entry_points={
          'console_scripts': [
              'screenless = screenless:main'
          ]
      },
      author = "Brendan Howell",
      author_email = "brendan@howell-ersatz.com",
      license = "GPLv3",
      url = "http://wintermute.org/brendan",
      include_package_data=True,
      classifiers = [
        "Development Status :: 4 - Beta",
	    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        'Operating System :: POSIX',
        'Programming Language :: Python',
         ],
     )

