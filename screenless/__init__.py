import sys

from . import mgmt

def main(args=None):
    if args is None:
        args = sys.argv[1:]

    print("initializing Screenless Office...")
    mgr = mgmt.Management()

    print("starting Management...")
    mgr.run()
