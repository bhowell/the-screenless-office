import evdev
from .bureau import bureau

KEYS = {
        0: None, 1: u'ESC', 2: u'1', 3: u'2', 4: u'3', 5: u'4', 6: u'5', 7: u'6', 8: u'7', 9: u'8',
        10: u'9', 11: u'0', 12: u'-', 13: u'=', 14: u'\b', 15: u'\t', 16: u'q', 17: u'w', 18: u'e', 19: u'r',
        20: u't', 21: u'y', 22: u'u', 23: u'i', 24: u'o', 25: u'p', 26: u'[', 27: u']', 28: u'\n', 29: u'LCTRL',
        30: u'a', 31: u's', 32: u'd', 33: u'f', 34: u'g', 35: u'h', 36: u'j', 37: u'k', 38: u'l', 39: u';',
        40: u'"', 41: u'`', 42: u'LSHFT', 43: u'\\', 44: u'z', 45: u'x', 46: u'c', 47: u'v', 48: u'b', 49: u'n',
        50: u'm', 51: u',', 52: u'', 53: u'/', 54: u'RSHFT', 56: u'LALT', 100: u'RALT'
}


class EvDevKb(bureau.Bureau):
    """Converts keyboard, barcode scanner, etc. input to messages """

    def __init__(self, name):
        Bureau.__init__(self, name)

        devices = map(evdev.InputDevice, evdev.list_devices())

        self.inputs = []

        # add anything that smells like a keyboard and grab focus
        for dev in devices:
            if evdev.ecodes.KEY_A in dev.capabilities()[evdev.ecodes.EV_KEY]:
                dev.grab()
                self.inputs.append(dev)

    def run_io(self):
        val = ""
        #TODO: this is crap, needs to be multi-threaded and have one
        #      such loop for each input
        for ev in inputs[0].read_loop():
            if ev.type == evdev.ecodes.EV_KEY:
                data = evdev.categorize(ev)
                if data.keystate == 1:
                    if data.scancode == 28:
                        print("sending barcode:", val)
                        val = ""
                    else:
                        try:
                            val += KEYS[data.scancode]
                        except KeyError:
                            print("Error invalid keycode:", data.scancode)

if __name__ == "__main__":
    evd = EvDevKb("evdevkb")
    evd.run()
