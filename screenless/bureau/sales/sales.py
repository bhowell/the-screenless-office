import glob
import os
import subprocess

from bureau import Bureau, add_command


class Sales(Bureau):
    """
    The sales department makes presentations to potential customers.
    """

    name = "Sales Office"
    prefix = "SA"
    version = 0

    def __init__(self):
        Bureau.__init__(self)
        try:
            self.db = os.mkdir("SA")
        except FileExistsError:
            pass  # we've already got a db folder

    @add_command("p", "Play Media")
    def play_media(self, data):
        """
        Shows media on a connected projector or plays audio.
        """
        searchpath = "SA/SAp." + data + "*"
        print("looking in ", searchpath)
        mediapath = glob.glob(searchpath)[0]
        print("playing", mediapath)
        cli = ["mplayer", "-fs", mediapath]
        jux = subprocess.Popen(cli)


def main():
    sa = Sales()
    sa.run()


if __name__ == "__main__":
    main()
