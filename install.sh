#!/usr/bin/env bash
sudo apt install vlc git python3-dev build-essential python3-pip python3-wheel python3-cffi python3-venv pmount fuse3 libjpeg-dev libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info cups libzmq3-dev fortunes-off cups-bsd liblmdb-dev tcl8.6-dev tk8.6-dev libxml2-dev libxslt1-dev uvccapture uvcdynctrl python-gst-1.0 gir1.2-gstreamer-1.0 gir1.2-gst-plugins-base-1.0 gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-plugins-bad gstreamer1.0-alsa libglib2.0-dev libgirepository1.0-dev libcairo2-dev gir1.2-totemplparser-1.0
if sudo apt install linux-headers-$(uname -r) ; then
	echo "installed kernel headers for debian"
else
	sudo apt install raspberrypi-kernel-headers
	echo "installed kernel headers for raspbian"
fi
git clone https://gitlab.com/bhowell/the-screenless-office.git
cd the-screenless-office
python3 -m venv .env
source .env/bin/activate
pip install -r requirements.txt
